<?php
namespace Drupal\indication_field_update\Commands;

use Drupal;
use Drush\Commands\DrushCommands;
use Exception;

/**
 * A Drush command file.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 * - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 * - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class FieldUpdate extends DrushCommands {

  /**
   * Updates the fields
   *
   * @bootstrap full
   * @command field_update:update_fields
   * @aliases update_fields
   * @usage field_update:update_fields
   *   Updates all the fields for Ábendingategundir
   * 
   * @throws Exception
   */
  public function updateFields() {
    indication_field_update_indications();
  }
}
