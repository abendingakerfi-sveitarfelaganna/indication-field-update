# Indication Field Update Module
This module was intended to run once on indication systems that had chosen instead of cshs module installed. Please follow these steps precicely and everything should run smooth.

1. Import a live dump of the database locally.
2. Run drush cex (config export) to confirm that all your live config is in config/sync folder. It's good to commit that to your git repo.
3. Fetch cshs module with composer and install it. Fetch the the indication_field_update module with git clone and put it in the modules/custom folder.
4. Change the "Sýsla með ábendingu" form view. Use cshs instead of Chosen.
5. Run drush cex to export the config you just did
6. Edit the field.storage.node.field_indication_type_ref.yml - Line 18: Set cardinality to -1
7. Edit the core.entity_form_display.node.indications.edit_indication.yml - Set save_linage: true
8. Run the manual changes by running drush cim (config import)
9. Hack the indication.module - Line 33, put if($node->bundle() == 'something_else_than_indication') { This is to make sure when we run the save command that all the hooks don't run and emails and more get sent out.
10. Run drush update_fields to find and connect taxonomies
11. Make sure CSHS is installed and save -lineage is really true
12. Fix display of categories in views so that multiple indications are unordered list and not "simple"
13. Uninstall the indication_field_update module
14. Export the config again to make sure all config is in config/sync
15. Remove the indication_field_update module from modules/custom. You don't need it anymore.
16. Dump the database locally and upload it to live.
